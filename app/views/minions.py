from dictor import dictor
from collections import OrderedDict
from quart import render_template
from loguru import logger
from app import app
from app.views.db import get_minions, get_highstate_totals, get_sync_date
from app.views.db.audit import get_audit_data
from app.views.db.generator import run_generator
from flask_login import login_required

@app.route('/minion_status')
@login_required
async def minion_status():
    ''' salt minion connection status '''
    
    minions = get_minions()

    if not minions:
        return await render_template('error.html', title='Exception', msg=str('No minion data available, please run Sync DB to generate a new database')), 500
    
    return await render_template("minions.html", title='Silica', sync=get_sync_date(), minions=minions, nav='minions') 

@app.route('/highstate')
@login_required
async def highstate():
    ''' show highstate details '''
    
    totals = get_highstate_totals()

    if not totals:
        return await render_template('error.html', title='Exception', msg=str('No minion data available, please run Sync DB to generate a new database')), 500

    return await render_template("highstate.html", title='Silica - Highstate', totals=totals, sync=get_sync_date(), nav='highstate')

@app.route('/generator')
@login_required
async def generator():
    ''' run Generator manually '''
    return await render_template("generator.html", title='Silica - Update DB', nav='generator')

@app.route('/start_generator')
@login_required
async def start_generator():
    ''' run Generator manually '''
    
    result = run_generator()
    
    if result == 'generator sync is complete.':
        return await render_template("generator.html", title='Silica - Update DB', nav='generator', result=result)   
    else:
        return await render_template("generator.html", title='Silica - Update DB', nav='generator')


@app.route('/minion_audit')
@login_required
async def minion_audit():
    ''' salt minion command history '''
    logger.warning('1')
    audit_data = get_audit_data()
    logger.warning('2')
    
    if not audit_data:
        return await render_template('error.html', title='Exception', msg=str('No Audit data available.')), 500
    
    audit_dict = {}
    
    for d in audit_data:
        d = d.__dict__
        audit_timestamp = dictor(d, 'timestamp')
        audit_dict[audit_timestamp] = {}
        audit_dict[audit_timestamp]['hostname'] = dictor(d, 'hostname')
        audit_dict[audit_timestamp]['username'] = dictor(d, 'username')
        audit_dict[audit_timestamp]['command'] = dictor(d, 'command')
        audit_dict[audit_timestamp]['args'] = dictor(d, 'args')
    return await render_template("audit.html", title='Silica Minion Audit', audit_dict=audit_dict, nav='audit') 