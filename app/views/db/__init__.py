import os
import sys
import json
from dictor import dictor
from loguru import logger
from tinydb import TinyDB, Query
from collections import Counter
import pendulum
from config import BASEDIR
from app.views.errors import unhandled_exception

db_minions = BASEDIR + '/app/views/db/minions.json'

def check_db():
    '''
    checks if a Minion DB has been created
    '''
    if not os.path.exists(db_minions):
        raise Exception('Minion DB file not found, please run Generator to create a new DB')


def get_minions():
    ''' get all minion information '''
    
    check_db()
    
    db = TinyDB(db_minions)

    minions = []

    for value in db.all():
        minions.append(dict(value))

    return minions   


def get_targets():
    '''
    returns list of minions that are active/connected
    '''

    db = TinyDB(db_minions)
    Q = Query()
    
    targets = []
    for target in db.search(Q.status == 'True'):
        targets.append(target['name'])
    return targets


def get_highstate():
    '''
    returns minion highstates
    '''

    db = TinyDB(db_minions)
    Q = Query()
    targets = []
    for target in db.search(Q.status == 'True'):
        targets.append(target['name'])
    return targets


def get_highstate_totals():
    '''
    gets all minions and their highstate results
    returns totals for Changes, No Changes and Failures
    '''
    totals = {}
    minions = get_minions()


    if not minions:
        return None
        
    for _min in minions:
        name = dictor(_min, 'name')

        if name:
            totals[name] = {}
            
            # temporary storage dicts to organize Changes, No Changes, Failures
            # on Highstate.html, will show Failures first, then Changes, then No Changes (sorted by alphabet)
            nochanges = {}
            changes = {}
            failures = {}

            summary = dictor(_min, 'highstate')
         
            if summary and isinstance(summary, dict):
                count_nochanges, count_changes, count_failures = 0,0,0
            
                for hsitem in summary:
                    try:
                        iname = summary[hsitem]['name']
                    except (KeyError, ValueError, IndexError, TypeError):
                        iname = False

                    if iname:
                        if summary[hsitem]['result'] is True:
                            count_nochanges += 1                           
                            nochanges[iname] = {
                                'name':     summary[hsitem]['name'],
                                'comment':  summary[hsitem]['comment'], 
                                'sls':      summary[hsitem]['__sls__']
                            }
                        if summary[hsitem]['result'] is None:
                            count_changes += 1
                            changes[iname] = {
                                'name':     summary[hsitem]['name'],
                                'comment':  summary[hsitem]['comment'], 
                                'sls':      summary[hsitem]['__sls__']
                            }
                        if summary[hsitem]['result'] is False:
                            count_failures += 1
                            failures[iname] = {
                                'name':     summary[hsitem]['name'],
                                'comment':  summary[hsitem]['comment'], 
                                'sls':      dictor(summary, f'{hsitem}.__sls__', 'this state file is unknown')
                            }

                # return Totals dict back to view, organize by change/nochange/falure count and 
                # change/nochange/failure summaries
                totals[name]['count_nochanges'] = count_nochanges
                totals[name]['count_changes'] = count_changes
                totals[name]['count_failures'] = count_failures
                totals[name]['nochanges'] = nochanges
                totals[name]['changes'] = changes
                totals[name]['failures'] = failures
                totals[name]['summary'] = summary
            else:
                logger.debug('4')
                totals[name]['summary'] = 'Unavailable'
    return totals


def get_sync_date():
    '''
    returns when DB generator was last run (N minutes ago)
    '''
    
    now = pendulum.now()
    db = TinyDB(db_minions)
    Q = Query()
    sync = db.search(Q.sync)[0]['sync']
    
    if sync:
        sync = pendulum.parse(sync)
        difference = now.diff(sync).in_minutes()
        return difference
    else:
        return "No DB data available, run DB Generator to create a new DB file"


def get_metrics_os():
    '''
    returns OS count by OS type
    '''
    os_count = []

    minions = get_minions()

    # remove sync information from list
    minions.pop(0)
    
    for minion in minions:
        
        if dictor(minion, 'grains.osfullname'):
             if dictor(minion, 'grains.osfullname'):
                 os_type = str(minion['grains']['osfullname']) + ' ' + str(minion['grains']['osrelease'])

        if os_type:
            os_count.append(os_type)
    
    counter = Counter(os_count)
    ret = []
    for os in counter:
        ret.append({"OS": os, "count": counter[os]})

    return ret


def get_metrics_inactive():
    '''
    returns all minions that are down
    '''
    inactive = []

    minions = get_minions()

    # remove sync information from list
    minions.pop(0)
    
    for minion in minions:
        
        if dictor(minion, 'status'):
             if dictor(minion, 'status') == 'False':
                 inactive.append(minion['name'])
    return inactive

