import os
import json
import sys
import pendulum
from sqlalchemy import create_engine, ForeignKey, desc
from sqlalchemy import Column, String, DateTime, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.orm import relationship, backref
from loguru import logger
from app import db

#from app.models import User
from config import BASEDIR, TIMEZONE, AUDIT_SCROLLBACK

#auth_dir = BASEDIR + '/app/views/auth'
db_audit = BASEDIR + '/app/views/db/audit.db'
engine = create_engine('sqlite:///{}'.format(db_audit), echo=True)
Base = declarative_base()

# create a Session
Session = sessionmaker(bind=engine)
session = Session()

class Audit(Base):
    __tablename__ = "audit"

    id = Column(Integer, primary_key = True)
    username = Column(String(64), index = False, unique = False)
    hostname = Column(String(128))
    command = Column(String(128))
    args = Column(String(128))
    timestamp = Column(DateTime, default=pendulum.now(TIMEZONE))

    def __init__(self, username, hostname, command, args, timestamp):
        self.username = username
        self.hostname = hostname
        self.command = command
        self.args = args
        self.timestamp = timestamp

def create_audit_db():
    '''
    populate User DB with Silica Admin user
    '''
    # create Users DB
    Base.metadata.create_all(engine)
    logger.info('create_audit: creating new Audit DB')

def add_audit(username, hostname, command, args):
    ''' add a new audit record '''
    
    if not os.path.exists(db_audit):
        create_audit_db()
    
    timestamp = pendulum.now(TIMEZONE)
    record = Audit(username, hostname, command, args, timestamp)
    session.add(record)
    
    # commit the record the database
    session.commit()

def get_audit_data():
    if not os.path.exists(db_audit):
        create_audit_db()
        return 'no audit information available'
        
    audit_log = session.query(Audit).order_by(desc('timestamp')).limit(AUDIT_SCROLLBACK).all()
    return audit_log

