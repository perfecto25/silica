import os
import json
import sys
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, Boolean
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.orm import relationship, backref
from werkzeug.security import generate_password_hash, check_password_hash
from loguru import logger
from pathlib import Path

app_path = Path(__file__).resolve().parents[3]

# append App to get Silica libs
sys.path.append(str(app_path))

#from app.models import User
from config import BASEDIR
from app.models import User
from app.views.db.user import Base

LOGDIR = BASEDIR + '/logs' 

if not os.path.exists(LOGDIR):
    os.makedirs(LOGDIR)

logger.add(LOGDIR + '/silica.log', 
    rotation='25MB', 
    colorize=True, 
    format="<green>{time:YYYYMMDD_HH:mm:ss}</green> | {level} | <level>{message}</level>",
    level="DEBUG")

#auth_dir = BASEDIR + '/app/views/auth'
db_users = BASEDIR + '/app/views/db/users.db'
engine = create_engine('sqlite:///{}'.format(db_users), echo=True)
#Base = declarative_base()



class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key = True)
    username = Column(String(64), index = True, unique = True)
    password = Column(String(128))
    email = Column(String(120), index = True, unique = True)
    admin = Column(Boolean, default=True, nullable=False)

    def __init__(self, username, email, password, admin):
        self.username = username
        self.email = email
        self.password = password
        self.admin = admin

def create_temporary_admin():
    '''
    populate User DB with Silica Admin user
    '''
    if os.path.exists(db_users):
      logger.warning('Users DB is already present. Delete ' + db_users + ' to create new temporary admin')
      sys.exit()

    # create Users DB
    Base.metadata.create_all(engine)

    logger.info('create_admin: inserting temporary Admin user into users.db')

    # create a Session
    Session = sessionmaker(bind=engine)
    session = Session()

    pw_hash = generate_password_hash('admin123')
    user = User('admin', 'admin@silica', pw_hash, 1)
    session.add(user)

    # commit the record the database
    session.commit()

    logger.info('auth: Admin user inserted into users.db')
    logger.warning('temporary username: admin')
    logger.warning('temporary password: admin123')
    logger.warning('login and change the administrator password')

if __name__ == "__main__":
    create_temporary_admin()