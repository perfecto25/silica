from sys import argv
import salt.client

local = salt.client.LocalClient()
minion = str(argv[1])
states = []

result = local.cmd(minion, 'state.show_highstate')

for dict in result[minion].itervalues():
    try:
        states.index(dict['__sls__'])
    except ValueError:
        states.append(dict['__sls__'])

print sorted(states)

# return all States that are applicable to minion