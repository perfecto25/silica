import json
import yaml
from dictor import dictor
from quart import render_template, flash, jsonify
from flask import request
from loguru import logger
from app import app
from app.views.cmd import get_client
from app.views.db import get_targets
from app.views.db.audit import add_audit
from flask_login import login_required, current_user

from config import ADMIN_CMDS, REGULAR_CMDS, NO_TEST, USER_OPTIONS

nav = 'run_cmd'

def get_CMDS():
    ''' returns a dict of allowed remoted execution commands based on user's access level '''
    if current_user.admin:
        CMDS = ADMIN_CMDS
    else:
        CMDS = REGULAR_CMDS
    return CMDS 

@app.route('/run_cmd')
@login_required
async def run_cmd():
    ''' 
    generate a form to enter a target and command
    '''
    target_list = get_targets()

    if target_list:
        return await render_template("run_cmd.html", title='Saltstack - Execute Command', cmd_list=get_CMDS(), target_list=target_list, nav=nav, options=USER_OPTIONS)
    else:
        return await render_template("msg.html", title="Saltstack - Execute Command", header="No active minions found", msg="All Minion agents are down. Run 'Sync DB' to generate fresh minion data", nav=nav)

@app.route('/run_cmd_execute', methods=['GET', 'POST'])
@login_required
async def run_cmd_execute():
    '''
    execute a remote Salt command on target 
    '''
    
    target_list = get_targets()

    if request.method == 'POST':

        cmd_name = request.form.get('cmd_name')
        hostname = request.form.get('hostname')
        args = request.form.get('args')
        simulate = request.form.get('simulate')
        output_fmt = request.form.get('output_fmt')

        if not hostname or hostname == '':
            return await render_template('run_cmd.html', title='Saltstack - Error', cmd_list=get_CMDS(), nav=nav, error_msg='You must provide a hostname', target_list=target_list, options=USER_OPTIONS)
 
        if not cmd_name or cmd_name == '':
            return await render_template('run_cmd.html', title='Saltstack - Error', cmd_list=get_CMDS(), nav=nav, error_msg='You must provide a command', target_list=target_list, options=USER_OPTIONS)
        
        if simulate == 'true':
            cmd_module = cmd_name.split('.')[0]
            if cmd_name not in NO_TEST and cmd_module not in NO_TEST:
                args = args + ' test=true'
            else:
                simulate = ''
                
        client = get_client()   
        result = client.run_cmd(hostname, cmd_name, args, run_async=False)
        result = dictor(result, hostname)

        if output_fmt == 'yaml':
            result = yaml.dump(result)
        else:
            result = json.dumps(result, indent=2)

        logger.info(f"executing cmd: salt {hostname} {cmd_name} {args}")
        add_audit(current_user.username, hostname, cmd_name, args)

        return await render_template('run_cmd.html', title='Saltstack - Cmd Results', action='display_cmd_results', result=result, cmd_list=get_CMDS(), hostname=hostname, cmd_name=cmd_name, args=args, simulate=simulate, output_fmt=output_fmt, nav=nav, target_list=target_list, options=USER_OPTIONS)
