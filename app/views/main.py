# VIEW MAIN

import json
from dictor import dictor
from app import app
from quart import render_template, session, redirect, url_for
from loguru import logger
from datetime import timedelta
from flask_login import login_required, logout_user, current_user
from functools import wraps
from app.views.db import get_metrics_os, get_metrics_inactive

def admin_only(func):
    ''' decorator to check if currently logged in user is an Admin, if not, redirect to home '''
    @wraps(func)
    def check_admin(*args, **kwargs):
        if not current_user.admin:
            return render_template("index.html", title='Silica')
        return func(*args, **kwargs)
    return check_admin

@app.before_request
def before_request():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=app.config['SESSION_LIFETIME'])

@app.route('/')
@app.route('/index')
@login_required
async def index():
   return redirect(url_for('dashboard'))

@app.route('/dashboard')
@login_required
async def dashboard():
    ''' 
    show statistics dashboard 
    '''
    metrics_os = get_metrics_os()
    metrics_inactive = get_metrics_inactive()
    return await render_template('dashboard.html', title='Dashboard', 
        metrics_os=metrics_os, 
        metrics_inactive=metrics_inactive, 
        nav='dashboard')


@app.route('/cheatsheet')
@login_required
async def cheatsheet():
    ''' 
    docs > cheatsheet 
    '''
    return await render_template("cheatsheet.html", title='Silica - Salt Cheatsheet', nav='docs') 

    