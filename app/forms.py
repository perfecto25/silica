from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextField, BooleanField
from wtforms.validators import DataRequired, EqualTo, Length, Email
from wtforms.fields.html5 import DateField, EmailField

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    #email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField("Log In")

class NewUserForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = EmailField('Email', [DataRequired(), Email(message=("Enter proper email address"))])
    password1 = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Password', validators=[DataRequired()])
    admin = BooleanField('Admin?')
    submit = SubmitField("Submit")

class ChangePWForm(FlaskForm):
    password1 = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField("Submit")