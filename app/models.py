from flask_login import UserMixin
from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin, db.Model):
    
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), index = True, unique = True)
    password = db.Column(db.String(128))
    email = db.Column(db.String(120), index = True, unique = True)
    admin = db.Column(db.Boolean, default=True, nullable=False)
    
    def __init__(self, username, email, password, admin):
        self.username = username
        self.email = email
        self.password = password
        self.admin = admin

    def __repr__(self):
        return '<User %r>' % self.email    
    
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False
        
    def get_id(self):
        return str(self.id)
    
    # def set_password(self, password):
    #     self.password_hash = generate_password_hash(password)

    def check_password(self, supplied_pw):
        return check_password_hash(self.password, supplied_pw)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))
