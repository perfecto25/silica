# Silica Installation

The following shows a setup using Centos7 and Python 3.6.7

### Prerequisites
Silica requires the following:
- Python 3.6 or higher
- Pip3 (gets installed with Python 3.6+)
- pipenv module
- Saltstack version Nitrogen (2017.7.0) or higher
- Salt-API service

---
### Installation Procedure
1. install python3.6 or higher
1. install Pipenv
1. add Silica service account
1. add systemd service script

---
#### Python 3.6
if you dont have Python higher than 3.5, install the following:

install Python 3.6 (as root or sudo)
```
yum install gcc openssl-devel bzip2-devel libffi libffi-devel
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum update
yum install -y python36u python36u-libs python36u-devel python36u-pip
```

check your python 3.6 version,
```
> python3.6 -V
Python 3.6.7
```

#### Pipenv
install Pipenv using pip3.6+

    root@saltmaster> pip3.6 install pipenv 


## add Silica service account  
On Salt Master, as root;
 
    root@saltmaster> useradd -d /home/silica silica
    
generate a password for this user

    root@saltmaster> passwd silica
    <enter password>

    
## install Silica
    root@saltmaster> cd /opt && git clone git@gitlab.com:perfecto25/silica.git

    # set file permissions
    root@saltmaster> chown -R silica:silica /opt/silica
    root@saltmaster> chmod 700 /opt/silica
    root@saltmaster> chmod 600 /opt/silica/config.py

    
    
    # install python dependencies using Pipenv
    root@saltmaster> su silica
    silica@saltmaster> pipenv install 

## Add Systemd Service script
as root, add the following to /etc/systemd/system/silica.service

(for ExecStart, make sure the Pipenv path matches your environment, ie /bin/pipenv, /usr/bin/pipenv, etc)

```
[Unit]
Description=Salt Silica Service
After=network.target

[Service]
User=silica
Restart=always
Type=simple
WorkingDirectory=/opt/silica
ExecStart=/usr/local/bin/pipenv run silica
StandardOutput=null

[Install]
WantedBy=multi-user.target
```
reload Daemon

    root@saltmaster> systemctl daemon-reload

Start Silica service

    root@saltmaster> systemctl start silica

Silica should now be running on this host via port thats configured in config.py

Navigate to http://SALTMASTER:PORT and you should now see a Login screen

![](../app/static/img/s01.png)

---
To complete the install, see [Configuration](docs/2_configuration.md) section for Salt-API configuation

