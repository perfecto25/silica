import os
VERSION = '0.0.7'
WTF_CSRF_ENABLED = True
SECRET_KEY = 'maytheshwartzbewithyou'
TIMEZONE = 'America/New_York'

# IP on which silica runs
HOST = '0.0.0.0'

# port on which app runs on
PORT = 5100  

# directory & file for overall logging
BASEDIR = os.getcwd()
LOGDIR = 'logs'
LOGFILE = 'silica.log'  

TEMPLATES_AUTO_RELOAD = True

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + BASEDIR + '/app/views/db/users.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# login session timeout after inactivity (in minutes)
SESSION_LIFETIME = 60

# Generator highstate interval (in minutes)
# This can also be run manually from the Console using "Sync DB"
# to disable generator, set INTERVAL to zero
INTERVAL = 0

SALT_API = {
    'user': 'silica',
    'pw': 'silica123',
    'host': 'localhost',
    'port': 9709,
    'allowed_users': 'root'
}


ENABLED_SECTIONS = {
    'remote_execution': True,
    'highstate': True,
    'minions': True
}

USER_OPTIONS = {
    'simulate': True,  #  default test=true
    'output': 'yaml'    # default remote exec command output (json, yaml)
}

# remote execution commands allowed for Admins only
ADMIN_CMDS = {
    'group': ['add', 'delete', 'info', 'adduser', 'deluser', 'chgid', 'getent', 'members'],
    'test': ['ping', 'version', 'versions', 'versions_report', 'echo'],
    'status': ['all_status', 'cpuinfo', 'cpustats', 'diskusage', 'meminfo', 'procs', 'version', 'vmstats', 'w', 'uptime'],
    'cmd': ['run'],
    'pkg': ['install', 'remove', 'version', 'upgrade', 'upgrade_available', 'list_pkgs', 'list_upgrades', 'refresh_db'],
    'service': ['start', 'stop', 'restart', 'reload', 'disable', 'enable', 'status', 'get_all', 'get_disabled', ],
    'grains': ['items', 'ls', 'get'],
    'user': ['list_users', 'add', 'delete', 'getent', 'info', 'list_groups', 'rename', 'chgid', 'chgroups'],
    'state': ['highstate', 'apply', 'sls'],
    'disk': ['percent', 'usage'], 
    'hosts': ['add_host', 'get_ip', 'list_hosts', 'rm_host', 'set_host'],
    'network': ['active_tcp', 'arp', 'connect', 'default_route', 'get_fqdn', 'get_hostname', 'get_route', 'hwaddr', 'interface', 'interfaces', 'ipaddrs', 'ping', 'routes', 'subnets', 'traceroute']
}

# remote execution commands allowed for non-Admins
REGULAR_CMDS = {
    'test': ['ping', 'version', 'versions', 'versions_report', 'echo'],
    'status': ['all_status', 'cpuinfo', 'cpustats', 'diskusage', 'meminfo', 'procs', 'version', 'vmstats', 'w', 'uptime'],
    'disk': ['percent', 'usage']
}

# commands that cannot run test=true
NO_TEST = [
    'test',
    'disk.percent', 'disk.usage'
    'group.getent',
    'grains', 
    'service.get_all', 
    'status', 
    'user.info', 'user.list_users', 'user.list_groups',
    'network',
    'hosts.list_hosts', 'hosts.get_ip'
    ]

# how many audit records to show
AUDIT_SCROLLBACK = 500